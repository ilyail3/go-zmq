#! /bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export GOPATH=$DIR

APP="zmq-example"

if [ -f "$DIR/bin/$APP" ]; then
    rm "$DIR/bin/$APP"
fi

go install $APP

RETVAL=$?
if [[ $RETVAL != 0 ]]; then
    exit $RETVAL
fi

if [ ! -f "$DIR/bin/$APP" ]; then
    echo "Compile didn't produce a file"
    exit 1
fi
