package main

import "log"
import "go-zmq"
import "os"


func main() {
    ctx, err := zmq.NewContext()

    if err != nil {
      panic(err)
    }

    defer ctx.Close()

    log.Println("Context ready")

    

    argsWithoutProg := os.Args[1:]

    if len(argsWithoutProg) != 1 {
        log.Fatal("Command missing")
    }

    command := argsWithoutProg[0]

    if command == "reply" {
        sock, err := ctx.Socket(zmq.Rep)

        if err != nil {
          panic(err)
        }
        
        defer sock.Close()

        if err = sock.Bind("tcp://*:5555"); err != nil {
          panic(err)
        }

        log.Println("socket ready")

        for {
            parts, err := sock.Recv()

            if err != nil {
                panic(err)
            }



            log.Printf("Received %d message parts", len(parts))

            if err = sock.Send(parts); err != nil {
                panic(err)
            }
        }
    } else if command == "request" {
        sock, err := ctx.Socket(zmq.Req)

        if err != nil {
          panic(err)
        }

        defer sock.Close()

        if err = sock.Connect("tcp://localhost:5555"); err != nil {
          panic(err)
        }

        err = sock.Send([][]byte{[]byte("Hello")})
        
        if err != nil {
            panic(err)
        }

        parts, err := sock.Recv()

        if err != nil {
            panic(err)
        }

        log.Printf("Received %d message parts", len(parts))

        for _, bytes := range parts {
            log.Printf("msg: %s", string(bytes))
        }
    } else {
        log.Fatal("Invalid command")
    }
}
